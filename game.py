#-*- coding:utf-8 -*-
#!/usr/bin/env python
import random
import copy
class game:
    table = [[0,0,0,0],
              [0,0,0,0],
              [0,0,0,0],
              [0,0,0,0]]
    score = 0
    def __init__(self):
        #self.table = [[8,4,4,2],
         #             [8,4,4,2],
          #            [0,0,0,0],
           #           [0,0,0,0]]
        self.newnumber()
        self.newnumber()
        self.draw()

    def getscore(self):
        return self.score
        
    def isover(self):
        over = True
        for i in xrange(0,4):
            for j in xrange(0,4):
                if self.table[i][j]==0:
                    over = False
                    break
                if i<3 and self.table[i+1][j] == self.table[i][j]:
                        over = False
                        break
                if j<3 and self.table[i][j+1] == self.table[i][j]:
                        over = False
                        break
            if not over:break
        #print over
        if over:
            #print "game over"
            return True
        else:
            return False

    def newnumber(self):
        flag = False
        for i in xrange(0,4):
            for j in xrange(0,4):
                if self.table[i][j]==0:
                    flag = True
                    break
            if flag:break
        while flag:
            x,y = random.randint(0,3),random.randint(0,3)
            if self.table[x][y]==0:
                self.table[x][y] = random.choice([2,2,2,4])
                break

    def draw(self):
        print 'score:',self.score
        for l in self.table:print l
        return self.table

    def up(self):
        temp = copy.deepcopy(self.table)
        for i in xrange(0,4):
            if self.table[0][i]==self.table[1][i] and self.table[2][i]==self.table[3][i] and 0 not in [self.table[m][i] for m in xrange(4)]:
                self.table[0][i],self.table[1][i],self.table[2][i],self.table[3][i] = (self.table[0][i]*2,self.table[2][i]*2,0,0)
                continue
            isjoin = False
            for j in xrange(0,4):
                current = j
                next = j-1
                for k in xrange(0,next+1):
                    if self.table[next][i] == 0:
                        self.table[current][i],self.table[next][i] = self.table[next][i],self.table[current][i]
                    elif isjoin:
                        break
                    elif not isjoin and self.table[current][i] == self.table[next][i]:
                        self.score += self.table[current][i]
                        self.table[next][i] = self.table[current][i]+self.table[next][i]
                        self.table[current][i] = 0
                        isjoin = True
                    current -= 1
                    next -= 1
        if temp!=self.table:self.newnumber()
        return self.draw()

    def down(self):
        temp = copy.deepcopy(self.table)
        for i in xrange(0,4):
            if self.table[0][i]==self.table[1][i] and self.table[2][i]==self.table[3][i] and 0 not in [self.table[m][i] for m in xrange(4)]:
                self.table[0][i],self.table[1][i],self.table[2][i],self.table[3][i] = (0,0,self.table[0][i]*2,self.table[2][i]*2)
                continue
            isjoin = False
            for j in xrange(-1,-5,-1):
                current = j
                next = j+1
                for k in xrange(next,0):
                    if self.table[next][i] == 0:
                        self.table[current][i],self.table[next][i] = self.table[next][i],self.table[current][i]
                    elif isjoin:
                        break
                    #print '%s,%s   %s,%s'%(i,j,self.table[i][current],self.table[i][next])
                    elif not isjoin and self.table[current][i] == self.table[next][i]:
                        self.score += self.table[current][i]
                        self.table[next][i] = self.table[current][i]+self.table[next][i]
                        self.table[current][i] = 0
                        isjoin = True
                    current += 1
                    next += 1
        if temp!=self.table:self.newnumber()
        return self.draw()



    def left(self):
        temp = copy.deepcopy(self.table)
        for i in xrange(0,4):
            if self.table[i][0]==self.table[i][1] and self.table[i][2]==self.table[i][3] and 0 not in self.table[i]:
                self.table[i] = [self.table[i][0]*2,self.table[i][2]*2,0,0]
                continue
            isjoin = False
            for j in xrange(0,4):
                current = j
                next = j-1
                for k in xrange(0,next+1):
                    if self.table[i][next] == 0:
                        self.table[i][current],self.table[i][next] = self.table[i][next],self.table[i][current]
                    elif isjoin:
                        break
                    elif not isjoin and self.table[i][current] == self.table[i][next]:
                        self.score += self.table[i][current]
                        self.table[i][next] = self.table[i][current]+self.table[i][next]
                        self.table[i][current] = 0
                        isjoin = True
                    current -= 1
                    next -= 1
        if temp!=self.table:self.newnumber()
        return self.draw()


    def right(self):
        temp = copy.deepcopy(self.table)
        for i in xrange(0,4):
            if self.table[i][0]==self.table[i][1] and self.table[i][2]==self.table[i][3] and 0 not in self.table[i]:
                self.table[i] = [0,0,self.table[i][0]*2,self.table[i][2]*2]
                continue
            isjoin = False
            for j in xrange(-1,-5,-1):
                current = j
                next = j+1
                for k in xrange(next,0):
                    if self.table[i][next] == 0:
                        self.table[i][current],self.table[i][next] = self.table[i][next],self.table[i][current]
                    elif isjoin:break
                    elif not isjoin and self.table[i][current] == self.table[i][next]:
                        self.score += self.table[i][current]
                        self.table[i][next] = self.table[i][current]+self.table[i][next]
                        self.table[i][current] = 0
                        isjoin = True
                    current += 1
                    next += 1
        if temp!=self.table:self.newnumber()
        return self.draw()


    def start(self):
        while 1:
            operation = raw_input('op')
            print operation
            if operation == ('a' or 'l'):self.left()
            elif operation == ('d' or 'r'):self.right()
            elif operation == ('w' or 'u'):self.up()
            elif operation == ('s' or 'd'):self.down()
            elif operation == 'e':break
            if self.isover():break

def main():
    g = game()
    g.start()


if __name__ == '__main__':
    main()
